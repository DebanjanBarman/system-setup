#!/bin/bash

#___________ Update the System ______________

echo "Updating the System"
sudo dnf update -y
echo "System updated successfully"


#___________ Install GNOME DE _______________

echo "Installing GNOME"
sudo dnf install gdm -y
echo "GNOME Installed"

#___________ Launch GNOME by default_________

sudo systemctl set-default graphical.target

#___________ Install wget, curl, vim ________

echo "Installing wget, curl and vim"
sudo dnf install wget curl vim -y
echo "Installed wget curl and vim "

#___________ Install GNOME Apps __________

echo "Installing GNOME Apps"
#sudo dnf install gnome-photos -y
sudo dnf install gnome-calculator -y
#sudo dnf install gnome-screenshot -y
sudo dnf install gnome-backgrounds -y
sudo dnf install gnome-calendar -y
sudo dnf install gnome-characters -y
sudo dnf install gnome-clocks -y
sudo dnf install gnome-control-center -y
sudo dnf install gnome-disks -y
sudo dnf install gnome-system-monitor -y
sudo dnf install gnome-font-viewer -y
sudo dnf install gnome-terminal -y
sudo dnf install gnome-help -y
sudo dnf install gnome-thumbnail-font -y
sudo dnf install gnome-tweaks -y
sudo dnf install gnome-text-editor -y
sudo dnf install gnome-weather -y
sudo dnf install gnome-logs -y
sudo dnf install nautilus -y
sudo dnf install rhythmbox -y
sudo dnf install evince -y
sudo dnf install baobab -y

echo "GNOME Apps installed"


#_______ Install Other Apps ________ 

#echo "Installing VsCodium"
#wget https://github.com/VSCodium/vscodium/releases/download/1.67.2/codium-1.67.2-1652920431.el7.x86_64.rpm -P /home/debanjan/Downloads/Programs
#sudo dnf install /home/debanjan/Downloads/Programs/codium-1.67.2-1652920431.el7.x86_64.rpm -y
#echo "VsCodium Installed"

#echo "Installing Emacs"
#sudo dnf install emacs -y
#echo "Emacs installed"

echo "Installing timeshift"
sudo dnf install timeshift -y
echo "Timeshift installed"

echo "Installing ProtonVPN"
wget https://protonvpn.com/download/protonvpn-stable-release-1.0.1-1.noarch.rpm -P /home/debanjan/Downloads/Programs
sudo dnf install /home/debanjan/Downloads/Programs/protonvpn-stable-release-1.0.1-1.noarch.rpm -y
sudo dnf install protonvpn -y
echo "ProtonVPN installed"

echo "Installing XDM"
wget https://github.com/subhra74/xdm/releases/download/7.2.11/xdm-setup-7.2.11.tar.xz -P /home/debanjan/Downloads/Programs
tar -xvf '/home/debanjan/Downloads/Programs/xdm-setup-7.2.11.tar.xz' -C /home/debanjan/Downloads/Programs
sudo /home/debanjan/Downloads/Programs/install.sh
echo "XDM installed"


echo "Installing Stacer"
wget https://github.com/oguzhaninan/Stacer/releases/download/v1.1.0/stacer-1.1.0-amd64.rpm -P /home/debanjan/Downloads/Programs
sudo dnf install /home/debanjan/Downloads/Programs/stacer-1.1.0-amd64.rpm -y

echo "Installing Pomatez"
wget https://github.com/roldanjr/pomatez/releases/download/v1.1.0/Pomatez-v1.1.0-linux.rpm -P /home/debanjan/Downloads/Programs
sudo dnf install /home/debanjan/Downloads/Programs/Pomatez-v1.1.0-linux.rpm -y

#___________________ Installing AppImage Launcher__________

echo "Installing Appimage launcher"
wget https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher-2.2.0-travis995.0f91801.x86_64.rpm -P /home/debanjan/Downloads/Programs/
sudo dnf install /home/debanjan/Downloads/Programs/appimagelauncher-2.2.0-travis995.0f91801.x86_64.rpm -y
echo "Appimage launcher installed"

#___________________ Setup AppImage Directory______________
cd
mkdir Applications


#____________________ AppImages _________________________


#___________________Install Todoist ___________________

#wget https://todoist.com/linux_app/appimage -P /home/debanjan/Applications

#__________________Change the owner of ~/Applications and ~/Downloads/Programs directory _________

sudo chown debanjan Applications/
sudo chown debanjan Applications/*

sudo chown debanjan Downloads/Programs/
sudo chown debanjan Downloads/Programs/*

#____________________FLATPAK APPS__________________________


echo "Installing Flatpak"
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo "Installing Firefox From Flathub"
flatpak install flathub org.mozilla.firefox -y

echo "Installing Brave Browser from FlatHub"
flatpak install flathub com.brave.Browser -y

echo "Installing VLC Media Player From FlatHub"
flatpak install flathub org.videolan.VLC -y

echo "Installing Telegram from FlatHub"
flatpak install flathub org.telegram.desktop -y

echo "Installing Eye of GNOME from Flathub"
flatpak install flathub org.gnome.eog -y

#echo "Installing Gnome Photos from Flathub"
#flatpak install flathub org.gnome.Photos -y

echo "Installing Emacs from Flathub"
flatpak install flathub org.gnu.emacs

echo "Installing ShotWell from Flathub"
flatpak install flathub org.gnome.Shotwell -y

echo "Installing QBitTorrent From FlatHub"
flatpak install flathub org.qbittorrent.qBittorrent -y

echo "Installing Chromium From Flathub"
flatpak install flathub org.chromium.Chromium -y

echo "Installing G4Music from Flathub"
flatpak install flathub com.github.neithern.g4music -y

echo "Installing Only Office Suite from Flathub"
flatpak install flathub org.onlyoffice.desktopeditors -y

echo "Installing Todoist from Flathub"
flatpak install flathub com.todoist.Todoist -y

# echo "Installing Planner"
# sudo flatpak install flathub com.github.alainm23.planner -y

# echo "Installing Flowtime from FlatHub"
# sudo flatpak install flathub io.github.diegoivanme.flowtime -y


#________________Install fonts _______________

echo "Installing Jetbrains mono fonts"
sudo dnf install jetbrains-mono-fonts-all -y

echo "Installing Bengali language fonts"
sudo dnf install langpacks-bn -y
sudo dnf install google-noto-sans-bengali-fonts -y
sudo dnf install google-noto-sans-bengali-ui-fonts -y
sudo dnf install google-noto-sans-bengali-ui-vf-fonts -y
sudo dnf install google-noto-sans-bengali-vf-fonts -y
sudo dnf install google-noto-serif-bengali-fonts -y
sudo dnf install google-noto-serif-bengali-vf-fonts -y
sudo dnf install lohit-bengali-fonts -y
sudo dnf install texlive-bengali -y

echo "Installing Roboto fonts"
sudo dnf install google-roboto-fonts -y
sudo dnf install google-roboto-condensed-fonts -y
sudo dnf install google-roboto-mono-fonts -y

# _____________ Advertisement/social-media/fake-news blocker_________

sudo curl https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn-social/hosts >> /etc/hosts
